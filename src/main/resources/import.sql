insert into plant (id, name, description, price) values (101, 'Bobcat E32i Excavator', '3.2 Tonne Compact excavator', 180);
insert into plant (id, name, description, price) values (102, 'Bobcat E35 Excavator', '3.4 Tonne Compact excavator', 200);
insert into plant (id, name, description, price) values (103, 'Bobcat E63 Excavator', '6.3 Tonne Midi excavator', 250);
insert into plant (id, name, description, price) values (104, 'Case CX75C SR Excavator', '7.4 Tonne Midi excavator', 300);
