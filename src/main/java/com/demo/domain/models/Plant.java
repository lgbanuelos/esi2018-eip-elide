package com.demo.domain.models;

import com.yahoo.elide.annotation.Include;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;

@Entity
@Include(rootLevel = true)
@Data
public class Plant {
    @Id
    public Long id;

    String name;
    String description;
    Integer price;
}
