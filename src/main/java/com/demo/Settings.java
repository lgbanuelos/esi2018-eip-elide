package com.demo;

import com.demo.domain.models.Plant;
import com.yahoo.elide.standalone.config.ElideStandaloneSettings;

public class Settings implements ElideStandaloneSettings {
    public String getModelPackageName() {
        return Plant.class.getPackage().getName();
    }
    public int getPort() {
        return 8088;
    }
}
